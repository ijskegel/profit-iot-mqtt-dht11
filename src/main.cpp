#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>

#define DHTPIN D3
#define DHTTYPE DHT11

const uint8_t ESP_LED = 2;

const char* ssid = "********";                    // Replace with correct ssid and password
const char* pass = "********";

const char* broker_id = "profit-iot";             // replace 'profit-iot' with your unique id
const char* broker = "test.mosquitto.org";
const char* countTopic = "/ijskegel/count";       // replace 'ijskegel' with your unique name
const char* ledTopic = "/ijskegel/led";
const char* humTopic = "/ijskegel/humidity";
const char* tempTopic = "/ijskegel/temperature";

WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE);

long currentTime = 0;
long lastDHTTime = 0;
long lastTime = 0;
long count = 0;
const int msgLength = 50;
char message[msgLength];

void switchLed(bool on) {
  if (on) {
    digitalWrite(ESP_LED, LOW);
  } else {
    digitalWrite(ESP_LED, HIGH);
  }
}

void setupWifi() {
  delay(100);
  Serial.print("Connecting to ");
  Serial.print(ssid);

  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }

  Serial.println(" : connected!");
}

// (re)connect to mqtt broker
void reconnect() {
  while (!client.connected()) {
    Serial.print("Connecting to ");
    Serial.print(broker);
    if (client.connect(broker_id /*, broker_user, broker_pass*/)) {
      Serial.println(" : connected!");
    } else {
      Serial.print(".");
      delay(5000);
    }
  }

  // subscribe to topics is only allowed when connected
  if (!client.subscribe(ledTopic)) {
    Serial.print("Subscribe to ");
    Serial.print(ledTopic);
    Serial.println(" failed!");
  }
}

// called when messages for subscribed topics are received
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Received topic ");
  Serial.print(topic);
  Serial.print(": ");
  for (unsigned int i = 0; i < length; ++i){
    Serial.print((char) payload[i]);
  }
  Serial.println();

  if (strcmp(topic, ledTopic) == 0) {
    // handle led switching
    if (length >= 1) {
      int value = payload[0] - '0';
      switchLed(value == 1);
    }
  } else {
    Serial.println("Topic not handled");
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(ESP_LED, OUTPUT);

  // turn led on during wifi connection
  switchLed(true);
  setupWifi();
  switchLed(false);

  dht.begin();

  // configure mqtt client
  client.setServer(broker, 1883);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // increase a counter every two seconds and publish it
  currentTime = millis();
  if (currentTime - lastTime > 2000) {
    count++;
    snprintf(message, 50, "%ld", count);
    Serial.print("Sending message: ");
    Serial.println(message);
    client.publish(countTopic, message);
    lastTime = millis();
  }

  currentTime = millis();
  if (currentTime - lastDHTTime > 10000) {
    float hum = dht.readHumidity();
    snprintf(message, msgLength, "%f", hum);
    Serial.print("Send ");
    Serial.print(humTopic);
    Serial.print(", message: ");
    Serial.println(message);
    client.publish(humTopic, message);

    float temp = dht.readTemperature();
    snprintf(message, msgLength, "%f", temp);
    Serial.print("Send ");
    Serial.print(tempTopic);
    Serial.print(", message: ");
    Serial.println(message);
    client.publish(tempTopic, message);

    lastDHTTime = millis();
  }
}
